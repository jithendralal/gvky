# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-10-21 11:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0008_auto_20171021_1101'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='member',
            options={'ordering': ('-generation', 'parent_id', 'sibling_order')},
        ),
        migrations.RemoveField(
            model_name='member',
            name='father',
        ),
        migrations.RemoveField(
            model_name='member',
            name='mother',
        ),
        migrations.AddField(
            model_name='member',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='mem_parent', to='family.Member'),
        ),
    ]
