# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-10-30 05:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0020_newsitem_event_date'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='articlephoto',
            options={'verbose_name': 'Gallery Photo'},
        ),
        migrations.AddField(
            model_name='member',
            name='icon',
            field=models.ImageField(blank=True, height_field='icon_height', help_text='This will be shown on lists. So use small pic.', max_length=255, null=True, upload_to='uploads', width_field='icon_width'),
        ),
        migrations.AddField(
            model_name='member',
            name='icon_height',
            field=models.IntegerField(blank=True, editable=False, null=True),
        ),
        migrations.AddField(
            model_name='member',
            name='icon_width',
            field=models.IntegerField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='home_page',
            field=models.BooleanField(default=False, help_text='Show this article in Home Page menu'),
        ),
        migrations.AlterField(
            model_name='article',
            name='picture',
            field=models.ImageField(blank=True, height_field='pic_height', help_text='If multiple photos are added in Gallery Photos below, then this photo is not needed.', max_length=255, null=True, upload_to='uploads', verbose_name='Article Photo', width_field='pic_width'),
        ),
        migrations.AlterField(
            model_name='member',
            name='picture',
            field=models.ImageField(blank=True, height_field='pic_height', help_text='This will be shown on details', max_length=255, null=True, upload_to='uploads', width_field='pic_width'),
        ),
    ]
