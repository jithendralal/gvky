# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-25 08:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0029_article_published_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newsitem',
            name='event_date',
            field=models.DateTimeField(blank=True, help_text='Date of event', null=True),
        ),
    ]
