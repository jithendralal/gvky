# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-10-22 02:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0004_auto_20171022_0151'),
    ]

    operations = [
        migrations.AddField(
            model_name='member',
            name='contact_number',
            field=models.CharField(blank=True, max_length=25, null=True),
        ),
    ]
