# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-11-21 11:15
from __future__ import unicode_literals

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0015_auto_20171119_0610'),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('url', models.CharField(max_length=255)),
                ('body', tinymce.models.HTMLField()),
            ],
        ),
    ]
