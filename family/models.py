# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from tinymce.models import HTMLField


class Feedback(models.Model):
    subject = models.CharField(max_length=255)
    body = HTMLField(verbose_name='message')
    name = models.CharField(max_length=255, verbose_name='Your name', blank=True, null=True,)
    picture = models.ImageField(upload_to='uploads', max_length=255,
                                width_field='pic_width', height_field='pic_height',
                                blank=True, null=True)
    pic_width = models.IntegerField(blank=True, null=True, editable=False)
    pic_height = models.IntegerField(blank=True, null=True, editable=False)
    reply =  HTMLField(blank=True, null=True)
    published_date = models.DateField(blank=True, null=True, auto_now_add=True)

    def __unicode__(self):
        return self.subject


class NewsItem(models.Model):
    title = models.CharField(max_length=255)
    body = HTMLField(verbose_name="Text")
    is_public = models.BooleanField(default=True)
    picture = models.ImageField(upload_to='uploads', max_length=255,
                                width_field='pic_width', height_field='pic_height',
                                blank = True, null = True)
    pic_width = models.IntegerField(blank=True, null=True, editable=False)
    pic_height = models.IntegerField(blank=True, null=True, editable=False)
    event_date = models.DateField(verbose_name="Date of news")
    name = models.CharField(max_length=255, verbose_name='Your name', blank=True, null=True,)

    def __unicode__(self):
        return self.title


class Banner(models.Model):
    title = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    body = HTMLField()

    def __unicode__(self):
        return self.title


class Article(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255, blank=True, null=True)
    author_details = models.CharField(max_length=255, blank=True, null=True)
    body = HTMLField()
    is_public = models.BooleanField(default=True)
    picture = models.ImageField(upload_to='uploads', max_length=255, verbose_name='Article Photo',
                                help_text='If multiple photos are added in Gallery Photos below, then this photo is not needed.',
                                width_field='pic_width', height_field='pic_height',
                                blank=True, null=True)
    pic_width = models.IntegerField(blank=True, null=True, editable=False)
    pic_height = models.IntegerField(blank=True, null=True, editable=False)
    home_page = models.BooleanField(default=False, help_text='Show this article in Home Page menu')
    published_date = models.DateTimeField(blank=True, null=True)

    def __unicode__(self):
        return self.title

    def photo_count(self):
        return self.articlephoto_set.all().count()


class ArticlePhoto(models.Model):
    article = models.ForeignKey(Article)
    caption = models.CharField(max_length=255, blank=True, null=True)
    picture = models.ImageField(upload_to='uploads', max_length=255,
                                width_field='pic_width', height_field='pic_height',
                                blank=True, null=True)
    pic_width = models.IntegerField(blank=True, null=True, editable=False)
    pic_height = models.IntegerField(blank=True, null=True, editable=False)

    class Meta():
        verbose_name = 'Gallery Photo'

    def __unicode__(self):
        ret = self.article.title
        if self.caption:
            ret = self.caption
        return ret

MALAYALAM_MONTHS = {
    'Chingam': u'ചിങ്ങം', 
    'Chingam-pdf': u'ചിങ്ങം',
    'Kanni': u'കന്നി', 
    'Kanni-pdf': u'കന്നി',
    'Thulam': u'തുലാം',
    'Thulam-pdf': u'തുലാം',
    'Vrishchikam': u'വൃശ്ചികം',
    'Vrishchikam-pdf': u'വൃശ്ചികം',
    'Dhanu': u'ധനു',
    'Dhanu-pdf': u'ധനു',
    'Makaram': u'മകരം',
    'Makaram-pdf': u'മകരം',
    'Kumbham': u'കുംഭം',
    'Kumbham-pdf': u'കുംഭം',
    'Meenam': u'മീനം ',
    'Meenam-pdf': u'മീനം ',
    'Medam': u"മേടം",
    'Medam-pdf': u" േമടം",
    'Idavam': u'ഇടവം',
    'Idavam-pdf': u'ഇടവം',
    'Mithunam': u'മിഥുനം',
    'Mithunam-pdf': u'മിഥുനം',
    'Karkkidakam': u'കർക്കിടകം',
    'Karkkidakam-pdf': u'കർക്കിടകം'
}

MONTHS = [
    ('Chingam', 'Chingam'),
    ('Kanni', 'Kanni'),
    ('Thulam', 'Thulam'),
    ('Vrishchikam', 'Vrishchikam'),
    ('Dhanu', 'Dhanu'),
    ('Makaram', 'Makaram'),
    ('Kumbham', 'Kumbham'),
    ('Meenam', 'Meenam'),
    ('Medam', 'Medam'),
    ('Idavam', 'Idavam'),
    ('Mithunam', 'Mithunam'),
    ('Karkkidakam', 'Karkkidakam')
]

STARS = [
    ('Aswathi','Aswathi'),
    ('Bharani','Bharani'),
    ('Karthika','Karthika'),
    ('Rohini','Rohini'),
    ('Makayiram','Makayiram'),
    ('Thiruvathira','Thiruvathira'),
    ('Punartham','Punartham'),
    ('Pooyam','Pooyam'),
    ('Ayilyam','Ayilyam'),
    ('Makam','Makam'),
    ('Pooram','Pooram'),
    ('Uthram','Uthram'),
    ('Atham','Atham'),
    ('Chithira','Chithira'),
    ('Chothi','Chothi'),
    ('Visakham','Visakham'),
    ('Anizham','Anizham'),
    ('Thrikketta','Thrikketta'),
    ('Moolam','Moolam'),
    ('Pooradam','Pooradam'),
    ('Uthradam','Uthradam'),
    ('Thiruvonam','Thiruvonam'),
    ('Avittam','Avittam'),
    ('Chathayam','Chathayam'),
    ('Pooruruttathi','Pooruruttathi'),
    ('Uthrittathi','Uthrittathi'),
    ('Revathi','Revathi'),
]

MALAYALAM_STARS = {
    'Aswathi': u'അശ്വതി',
    'Aswathi-pdf': u'അശ്വതി',
    'Bharani': u'ഭരണി',
    'Bharani-pdf': u'ഭരണി',
    'Karthika': u'കാർത്തിക',
    'Karthika-pdf': u'കാർത്തിക',
    'Rohini': u'രോഹിണി',
    'Rohini-pdf': u'േരാഹിണി',
    'Makayiram': u'മകയിരം',
    'Makayiram-pdf': u'മകയിരം',
    'Thiruvathira': u'തിരുവാതിര',
    'Thiruvathira-pdf': u'തിരുവാതിര',
    'Punartham': u'പുണർതം',
    'Punartham-pdf': u'പുണർതം',
    'Pooyam': u'പൂയം',
    'Pooyam-pdf': u'പൂയം',
    'Ayilyam': u'ആയില്യം',
    'Ayilyam-pdf': u'ആയില്യം',
    'Makam': u'മകം',
    'Makam-pdf': u'മകം',
    'Pooram': u'പൂരം',
    'Pooram-pdf': u'പൂരം',
    'Uthram': u'ഉത്രം',
    'Uthram-pdf': u'ഉത്രം',
    'Atham': u'അത്തം',
    'Atham-pdf': u'അത്തം',
    'Chithira': u'ചിത്തിര',
    'Chithira-pdf': u'ചിത്തിര',
    'Chothi': u'ചോതി',
    'Chothi-pdf': u'ചോതി',
    'Visakham': u'വിശാഖം',
    'Visakham-pdf': u'വിശാഖം',
    'Anizham': u'അനിഴം', 
    'Anizham-pdf': u'അനിഴം',
    'Thrikketta': u'തൃക്കേട്ട',
    'Thrikketta-pdf': u'േകട്ട',
    'Moolam': u'മൂലം',
    'Moolam-pdf': u'മൂലം',
    'Pooradam': u'പൂരാടം',
    'Pooradam-pdf': u'പൂരാടം',
    'Uthradam': u'ഉത്രാടം',
    'Uthradam-pdf': u'ഉത്രാടം',
    'Thiruvonam': u'തിരുവോണം',
    'Thiruvonam-pdf': u'തിരുവോണം',
    'Avittam': u'അവിട്ടം',
    'Avittam-pdf': u'അവിട്ടം',
    'Chathayam': u'ചതയം',
    'Chathayam-pdf': u'ചതയം',
    'Pooruruttathi': u'പൂരൂരുട്ടാതി',
    'Pooruruttathi-pdf': u'പൂരൂരുട്ടാതി',
    'Uthrittathi': u'ഉത്തൃട്ടാതി',
    'Uthrittathi-pdf': u'ഉത്തൃട്ടാതി',
    'Revathi': u'രേവതി',
    'Revathi-pdf': u'േരവതി',
}

MONTH_LIST = ['January', 'February', 'March', 'April', 'May', 'June',
              'July', 'August', 'September', 'October', 'November', 'December']

ROLES = [
    ('Secretary', 'Secretary'),
    ('Joint Secretary', 'Joint Secretary'),
    ('President', 'President'),
    ('Vice President', 'Vice President'),
    ('Treasurer', 'Treasurer'),
    ('Patron', 'Patron'),
]

GENS = [
    (1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)
]

BLOODS = [
    ('A+','A+'),
    ('B+','B+'),
    ('O+','O+'),
    ('AB+','AB+'),
    ('A-','A-'),
    ('B-','B-'),
    ('O-','O-'),
    ('AB-','AB-')
]

class Member(models.Model):
    name = models.CharField(max_length=255)
    nick_name = models.CharField(max_length=25, blank=True, null=True)
    contact_number = models.CharField(max_length=25, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    twitter = models.CharField(max_length=255, blank=True, null=True)
    facebook = models.CharField(max_length=255, blank=True, null=True)
    blood_group = models.CharField(max_length=3, blank=True, null=True, choices=BLOODS)
    address = HTMLField(blank=True, null=True)
    official_address = HTMLField(blank=True, null=True)
    location = models.CharField(max_length=255, blank=True, null=True)
    qualification = models.CharField(max_length=255, blank=True, null=True)
    job = models.CharField(max_length=255, blank=True, null=True)
    role = models.CharField(max_length=50, choices=ROLES, blank=True, null=True)

    date_of_birth = models.DateField(blank=True, null=True, help_text='format: dd-mm-yyyy')
    dob_day = models.IntegerField(blank=True, null=True, editable=False)
    dob_month = models.IntegerField(blank=True, null=True, editable=False)
    dob_monthname = models.CharField(blank=True, null=True, editable=False, max_length=25)
    dob_year = models.IntegerField(blank=True, null=True, editable=False)

    birth_star = models.CharField(max_length=25, choices=STARS, blank=True, null=True)
    birth_month = models.CharField(max_length=25, choices=MONTHS, blank=True, null=True)

    wedding = models.DateField(blank=True, null=True, help_text='format: dd-mm-yyyy')
    wed_day = models.IntegerField(blank=True, null=True, editable=False)
    wed_month = models.IntegerField(blank=True, null=True, editable=False)
    wed_monthname = models.CharField(blank=True, null=True, editable=False, max_length=25)
    wed_year = models.IntegerField(blank=True, null=True, editable=False)

    parent = models.ForeignKey('self', blank=True, null=True, related_name='mem_parent')
    spouse = models.ForeignKey('self', blank=True, null=True, related_name='mem_spouse')
    generation = models.PositiveIntegerField(choices=GENS, blank=True, null=True)
    sibling_order = models.PositiveIntegerField(choices=GENS, blank=True, null=True,
                                                help_text='Order among siblings. eg. 1 for Renu 2 for Resmi')

    picture = models.ImageField(upload_to='uploads', max_length=255, help_text='This will be shown on details',
                                width_field='pic_width', height_field='pic_height',
                                blank=True, null=True)
    pic_width = models.IntegerField(blank=True, null=True, editable=False)
    pic_height = models.IntegerField(blank=True, null=True, editable=False)

    icon = models.ImageField(upload_to='uploads', max_length=255, help_text='This will be shown on lists. So use small pic.',
                                width_field='icon_width', height_field='icon_height',
                                blank=True, null=True)
    icon_width = models.IntegerField(blank=True, null=True, editable=False)
    icon_height = models.IntegerField(blank=True, null=True, editable=False)

    class Meta():
        ordering = ('generation', 'parent_id', 'sibling_order')

    def __unicode__(self):
        return self.name

    def children(self):
        return Member.objects.filter(parent=self).order_by('sibling_order')

    def save(self):
        if self.date_of_birth:
            self.dob_day = self.date_of_birth.day
            self.dob_month = self.date_of_birth.month
            self.dob_monthname = MONTH_LIST[self.dob_month-1]
            self.dob_year = self.date_of_birth.year

        if self.wedding:
            self.wed_day = self.wedding.day
            self.wed_month = self.wedding.month
            self.wed_monthname = MONTH_LIST[self.dob_month-1]
            self.wed_year = self.wedding.year

        if self.parent:
            self.generation = self.parent.generation + 1
        return super(Member, self).save()

    def is_youngest(self):
        ret = False
        sibling = Member.objects.filter(parent=self.parent).order_by('sibling_order').last()
        if sibling.id == self.id:
            ret = True
        return ret

    def malayalam_star(self):
        ret = ''
        if self.birth_star:
            ret = MALAYALAM_STARS[self.birth_star]
        return ret.encode('utf-8')

    def malayalam_month(self):
        ret = ''
        if self.birth_month:
            ret = MALAYALAM_MONTHS[self.birth_month]
        return ret.encode('utf-8')

    def malayalam_month_pdf(self):
        ret = ''
        if self.birth_month:
            ret = MALAYALAM_MONTHS[self.birth_month+'-pdf']
        return ret.encode('utf-8')

    def malayalam_star_pdf(self):
        ret = ''
        if self.birth_star:
            ret = MALAYALAM_STARS[self.birth_star+'-pdf']
        return ret.encode('utf-8')

