
$(document).ready(function(){

    $('#right_photo').click(function(){
        var id = $('#current_photo').val();
        var next = $('#'+id).next();
        var next_id = $(next).attr('id');
        var next_ph = $(next).val();
        var title = $(next).attr('title');
        $('#id_image').attr('src', next_ph);
        $('#id_image').attr('alt', next_ph);
        $('#current_photo').val(next_id);
        $('#counter').html(title);
    });

    $('#left_photo').click(function(){
        var id = $('#current_photo').val();
        var prev = $('#'+id).prev();
        var prev_id = $(prev).attr('id');
        if (!prev_id) {
            var prev_id = $('#last_id').val();
            var prev = $('#'+prev_id);
        }
        var prev_ph = $(prev).val();
        var title = $(prev).attr('title');
        $('#id_image').attr('src', prev_ph);
        $('#id_image').attr('alt', prev_ph);
        $('#current_photo').val(prev_id);
        $('#counter').html(title);
    });

});
