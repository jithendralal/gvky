
show_popup = function(id){
    $('#popup_body').html('');
    $('#popup_title_text').html($('#'+id).attr('title'));

    $.get("/family/get_member_data/?id="+id, function(data) {
      $('#popup_body').html(data);
      $('#popup').show().center();
      $('#member_image').show();
    });
}

show_photo_popup = function(src, width){
    $('#photo_popup_image').attr('src','');
    $('#photo_popup_image').attr('src',src);
    $('#photo_popup_image').css('width', (width*50/100));
    $('#photo_popup').show().center();
}

$(document).ready(function(){
    $('.node-span').click(function(){
        $('#loader').show().center();
        show_popup($(this).attr('id'));
    });

    $('#popup').hide();

    $('#popup_close').click(function(){
        $('#loader').hide();
        $('#popup').hide();
        $('#photo_popup').hide();
    });

    $('#photo_popup').hide();    

    $('#photo_popup_close').click(function(){
        $('#loader').hide();
        $('#photo_popup').hide();
    });

    $('body').on('click', '#member_image', function(){ 
        var width = $(window).width()
        show_photo_popup($(this).attr('src'), width);
    });
});
