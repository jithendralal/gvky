jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
    return this;
}

$(document).ready(function(){
    $('a').click(function(){
        $('#loader').show().center()
    });

    $('label[for="id_picture"').html('Select file:');

    $('#id_picture').parent().append('<span id="show_filename">No file selected</span>')

    $('#id_picture').bind('change', function() {
        var fileName = '';
        fileName = $(this).val().replace('C:\\fakepath\\', '');
        $('#show_filename').html(fileName);
    });
});
