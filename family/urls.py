from django.conf.urls import url
from views import news_list
from views import news
from views import article
from views import articles
from views import photos
from views import tree
from views import births
from views import members, members_list, family
from views import get_member_data
from views import set_dobs
from views import blood_groups
from views import about
from views import feedback
from views import messages
from views import name_search


urlpatterns = [
    url(r'^news_list/$', news_list, name='news-list'),
    url(r'^news/(\d+)/$', news, name='news'),
    url(r'^articles/$', articles, name='articles'),
    url(r'^article/(\d+)/$', article, name='article'),
    url(r'^photos/(\d+)/$', photos, name='photos'),
    url(r'^tree/$', tree, name='tree'),
    url(r'^get_member_data/$', get_member_data, name='member-data'),
    url(r'^births/([a-z0-9]+)/$', births, name='births'),
    url(r'^members/$', members, name='members'),
    url(r'^set_dobs/$', set_dobs),
    url(r'^blood_groups/$', blood_groups, name='blood_groups'),
    url(r'^members_list/$', members_list, name='members-list'),
    url(r'^about/$', about, name='about'),
    url(r'^feedback/$', feedback, name='feedback'),
    url(r'^family/([0-9]+)/$', family, name='family'),
    url(r'^messages/$', messages, name='messages'),
    url(r'^name_search/$', name_search, name='name-search'),
]
