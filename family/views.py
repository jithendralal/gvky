# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.template import loader
from models import NewsItem as News
from models import Article, ArticlePhoto, Member, MONTH_LIST, Feedback, NewsItem
from django.contrib.auth.decorators import login_required
import datetime
import cStringIO as StringIO
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.template.loader import get_template
from django import forms
from django.conf import settings
MEDIA_ROOT = settings.MEDIA_ROOT
from django.db.models import Q


def handle_uploaded_file(f):
    destination = open(MEDIA_ROOT + f.name, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    return


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = '__all__'
        exclude = ['reply']


class NewsForm(forms.ModelForm):
    class Meta:
        model = NewsItem
        fields = '__all__'
        exclude = ['is_public', 'pic_width', 'pic_height']


@login_required
def members_list(request):
    members = Member.objects.all().order_by('name')
    template_name = 'family/members_list.html'
    data = {'members': members}
    return render(request, template_name, data)


def get_client_ip(request):
    ip = ''
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


@login_required
def about(request):
    context = {}
    template = 'family/about.html'
    from django.contrib.gis.geoip2 import GeoIP2
    ip = get_client_ip(request)
    g = GeoIP2()
    city = None
    try:
        city = g.city(ip)
    except:
        pass
    context = {'location': city}
    return render(request, template, context)


@login_required
def news_list(request):
    message = ''
    error = ''
    form = NewsForm()
    if request.method == 'POST':
        form = NewsForm(request.POST, request.FILES)
        message = 'News not saved.'
        error = True
        if form.is_valid():
            form.save()
            message = 'News saved.'
            error = False
            try:
                picture = request.FILES['picture']
                handle_uploaded_file(picture)
            except:
                pass
            form = NewsForm()
    news = News.objects.filter(is_public=True).order_by('-event_date')
    context = {'news_list': news, 'form': form, 'message': message, 'error': error}
    template = 'family/news_list.html'
    return render(request, template, context)


@login_required
def news(request, id=None):
    newsitem = News.objects.get(pk=id)
    context = {'newsitem': newsitem}
    template = 'family/news.html'
    return render(request, template, context)


@login_required
def messages(request):
    messages = Feedback.objects.order_by('-id')
    context = {'messages': messages}
    template = 'family/messages.html'
    return render(request, template, context)


@login_required
def articles(request):
    articles = Article.objects.filter(is_public=True).exclude(home_page=True).order_by('published_date')
    context = {'articles': articles}
    template = 'family/articles.html'
    return render(request, template, context)


@login_required
def article(request, id=None):
    article = Article.objects.get(pk=id)
    context = {'article': article}
    template = 'family/article.html'
    count = article.photo_count()
    if count:
        template = 'family/photos.html'
        photo = ArticlePhoto.objects.filter(article_id=id).order_by('id')[0]
        context['photo'] = photo
        context['photos'] = ArticlePhoto.objects.filter(article_id=id).order_by('id').values('picture', 'id')
    return render(request, template, context)


@login_required
def photos(request, photo_id=None):
    next = request.GET.get('next', None)
    prev = request.GET.get('prev', None)
    photo = ArticlePhoto.objects.get(pk=photo_id)
    photo1 = None
    try:
        if prev:
            photo1 = ArticlePhoto.objects.filter(article=photo.article).order_by('id').filter(id__lt=photo_id).last()
        elif next:
            photo1 = ArticlePhoto.objects.filter(article=photo.article).order_by('id').filter(id__gt=photo_id).first()
    except:
        pass
    if photo1:
        photo = photo1
    article = photo.article
    context = {'photo': photo, 'article': article}
    template = 'family/photos.html'
    return render(request, template, context)


@login_required
def get_photo(request):
    next = request.GET.get('next', None)
    prev = request.GET.get('prev', None)
    photo_id = request.GET.get('photo_id', None)
    photo = ArticlePhoto.objects.get(pk=photo_id)
    photo1 = None
    try:
        if prev:
            photo1 = ArticlePhoto.objects.filter(article=photo.article).order_by('id').filter(id__lt=photo_id).last()
        elif next:
            photo1 = ArticlePhoto.objects.filter(article=photo.article).order_by('id').filter(id__gt=photo_id).first()
    except:
        pass
    if photo1:
        photo = photo1
    article = photo.article
    context = {'photo': photo, 'article': article}
    template = 'family/photos.html'
    return render(request, template, context)



def make_tree(root):
    children = root.children()
    child_list = []
    if children.count():
        for child in children:
            tree = make_tree(child)
            child_list.append(tree)
    return {root: child_list}


def traverse_tree(child_list):
    html = ''
    if len(child_list):
        for child_dict in child_list:
            member = child_dict.keys()[0]
            div = ''

            generation = member.generation - 1
            margin = generation * 10

            div += '<div class="node">'

            i = 1
            while i < member.generation:
                if member.is_youngest():
                    if i == member.generation - 2 and member.parent.is_youngest() \
                            or i == member.generation - 3 and member.parent.parent.is_youngest():
                        div += '<span class="node-connector gen" style="margin-left:20px;"></span>'
                    else:
                        div += '<span class="node-connector gen%s" style="margin-left:20px;"></span>' % (i + 1)
                else:
                    if i == member.generation - 2 and member.parent.is_youngest() \
                            or i == member.generation - 3 and member.parent.parent.is_youngest():
                        div += '<span class="node-connector gen" style="margin-left:20px;"></span>'
                    else:
                        div += '<span class="node-connector gen%s" style="margin-left:20px;"></span>' % (i + 1)
                i += 1

            div += '<span class ="gen%s-conn">&nbsp;</span>' % member.generation

            div += '<span class="node-container">'

            div += '<span class="node-span" id="%s" title="%s">' % (str(member.id), member.nick_name)
            div += '<span class="gen%s">' % member.generation
            div += member.nick_name or member.name
            if member.icon:
                div += ' <img style="width:20px;" src="%s" alt="pic">' %  member.icon.url
            div += '</span>'
            div += '</span>'

            spouse = member.spouse
            if spouse:
                div += '<span class="node-span" id="%s" title="%s">' % (str(spouse.id), member.spouse.nick_name)
                div += '<span class="gen%s">' % member.generation
                div += spouse.nick_name or spouse.name
                if spouse.icon:
                    div += ' <img style="width:20px;" src="%s" alt=""> ' % spouse.icon.url
                div += '</span>'
                div += '</span>'

            div += '</span>'
            
            div += '</div>'

            tr_list = child_dict[member]
            html += div
            html += traverse_tree(tr_list)
    return html


@login_required
def tree(request):
    root_member = Member.objects.filter(generation=1)[0]
    tree = make_tree(root_member)

    html = '<div class="node">'
    
    html += '<span class="node-container">'
    
    html += '<span id="%s" title="%s" class="node-span gen1">%s' % (str(root_member.id), root_member.nick_name, root_member.nick_name)
    if root_member.icon:
        html += ' <img style="width:20px;" src="%s" alt="pic">' % root_member.icon.url
    html += '</span>'

    spouse = root_member.spouse
    if spouse:
        html += '<span id="%s" title="%s" class="node-span gen1">' % (str(spouse.id), spouse.name)
        html += spouse.nick_name or spouse.name
        if spouse.icon:
            html += ' <img style="width:20px;" src="%s" alt=""> ' % spouse.icon.url
        html += '</span>'

    html += '</span>'

    html += '</div>'

    html += traverse_tree(tree[root_member])
    context = {'tree': html}
    template = template = 'family/tree.html'
    return render(request, template, context)


# ajax method
def get_member_data(request):
    id = request.GET.get('id')
    member = Member.objects.get(pk=id)
    template = 'family/member_data.html'
    context = {'member': member}
    return render(request, template, context)

# ajax
def name_search(request):
    srch = request.GET.get('srch')
    members = Member.objects.filter(Q(name__icontains=srch) | Q(nick_name__icontains=srch))
    template = 'family/name_search.html'
    context = {'members': members}
    return render(request, template, context)


def get_month_end(year, month):
    day = 31
    end = None
    while 1:
        try:
            end = datetime.datetime(year, month, day, 23, 59, 59)
            break
        except:
            day -= 1
    return end


def create_calendar(year, month):
    calendar = {}
    start = datetime.datetime(year, month, 1)
    startday = start.weekday()
    empty_days = []
    if startday < 6:
        empty_days = range(0, startday+1)
    end = get_month_end(year, month)
    cur = start.date()
    while cur <= end.date():
        calendar[cur] = []
        cur += datetime.timedelta(days=1)
    return start, empty_days, calendar


def populate_calendar(calendar, members, current_year):
    for member in members:
        day = datetime.date(current_year, member['dob_month'], member['dob_day'])
        calendar[day].append(member)
    return calendar


@login_required
def births(request, month=None):
    today = datetime.date.today()
    current_month = today.month
    current_year = today.year
    members = None
    calendar = None
    empty_days = 0
    start = today

    if month == 'init':
        month = current_month
    else:
        try:
            month = int(month)
        except:
            pass

    if month in range(1, 13):
        members = Member.objects.filter(
            date_of_birth__isnull=False, dob_month=month).order_by('dob_month', 'dob_day', 'dob_year').values(
            'nick_name', 'date_of_birth', 'dob_monthname', 'icon', 'name', 'dob_month', 'dob_day', 'dob_year')
        start, empty_days, calendar = create_calendar(current_year, month)
        calendar = populate_calendar(calendar, members, current_year)
        calendar = sorted(calendar.iteritems())
    elif month=='all':
        members = Member.objects.filter(
            date_of_birth__isnull=False).order_by('dob_month', 'dob_day', 'dob_year').values(
            'nick_name', 'date_of_birth', 'dob_monthname', 'icon', 'name', 'dob_month', 'dob_day', 'dob_year')
    template = 'family/births.html'

    months = {}
    for m in range(1, 13):
        month_day = datetime.date(current_year, m, 1)
        months[m] = month_day
    
    context = {'members': members, 'current_month': current_month, 'today': today,
               'current_year': current_year, 'selected': month,
               'calendar': calendar, 'empty_days': empty_days, 'start': start, 'months': months}
    return render(request, template, context)


@login_required
def set_dobs(request):
    members = Member.objects.filter(date_of_birth__isnull=False)
    for m in members:
        m.save()


@login_required
def members(request):
    members = Member.objects.all().order_by('nick_name').values('nick_name', 'contact_number', 'icon', 'name', 'job',
                                                                   'role', 'address', 'official_address', 'email')

    alpha = []
    for m in members:
        letter = m['nick_name'][0].lower()
        if letter not in alpha:
            alpha.append(letter)
    
    template = 'family/members.html'
    context = {'members': members, 'letters': alpha}
    return render(request, template, context)


@login_required
def blood_groups(request):
    members = Member.objects.exclude(blood_group='')
    group_list = ['O+', 'B+', 'A+', 'AB+', 'O-', 'B-', 'A-', 'AB-']
    groups = {'O+': [[], 0, 'opos'],
              'B+': [[], 0, 'bpos'],
              'A+': [[], 0, 'apos'],
              'AB+': [[], 0, 'abpos'],
              'O-': [[], 0, 'oneg'],
              'B-': [[], 0, 'bneg'],
              'A-': [[], 0, 'aneg'],
              'AB-': [[], 0, 'abneg']}
    for member in members:
        if member.blood_group:
            groups[member.blood_group][0].append(member)
            groups[member.blood_group][1] += 1

    blood = '<div id="blood">Our Family<br>'
    for key in group_list:
        g = groups[key]
        if g[1]:
            blood += '<span class="blood">' + '&nbsp;' * g[1] + '</span> ' + key + ' (' + str(g[1]) + ')<br>'
    blood += '<div style="text-align:right;">Approximate Graph</div></div>'

    template = 'family/blood_groups.html'
    context = {'groups': groups, 'keys': group_list, 'graph': blood}
    return render(request, template, context)


@login_required
def feedback(request):
    template = 'family/feedback.html'
    message = ''
    form = FeedbackForm()
    if request.method == 'POST':
        form = FeedbackForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            try:
                picture = request.FILES['picture']
                handle_uploaded_file(picture)
            except:
                pass
            message = 'Thank you for your message. Your message is listed in: <b><a href="/family/messages/">Messages</a></b>'
            form = FeedbackForm()
    context = {'form': form, 'message': message}
    return render(request, template, context)


def get_family(root):
    children = root.children()
    child_list = []
    if children.count():
        for child in children:
            tree = get_family(child)
            child_list += tree
    return {root: child_list}


def family(request, root_id):
    root = Member.objects.get(pk=root_id)
    tree = get_family(root)
    template = 'family/family.html'
    context = {'family': tree}
    return render(request, template, context)
