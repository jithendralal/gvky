from django.conf.urls import url, include
from django.contrib import admin
from main.views import home
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import login
from django.contrib.auth.views import logout

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^$', home, name='home'),
    url(r'^family/', include('family.urls')),
    url(r'^accounts/login/$', login, {'template_name': 'main/login.html'}),
    url(r'^accounts/logout/$', logout, {'template_name': 'main/index.html'}),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
