from django.shortcuts import render
from family.models import Article, Member
import datetime

def home(request):
    articles = Article.objects.filter(home_page=True).values('title','id')
    context = {'articles': articles}
    today = datetime.date.today()
    members = Member.objects.filter(date_of_birth=today)
    if members.count():
        context['members'] = members
    template = 'main/index.html'
    return render(request, template, context)
